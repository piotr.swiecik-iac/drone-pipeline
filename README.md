# drone-pipeline



## Overview

Lightweight CICD toolkit based on DroneCI. Can be tested locally with Docker Compose or deployed to AWS EC2 instance
(example Terraform script included).


## Requirements
x


## Running locally
- create ngrok domain (cloud edge -> domains)
- install ngrok agent
- add token
- start agent `ngrok http --domain=your.domain 8080`
